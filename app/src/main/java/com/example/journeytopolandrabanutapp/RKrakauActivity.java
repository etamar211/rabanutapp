package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RKrakauActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rkrakau);

        Button mosheIserlishButt = (Button) findViewById(R.id.button_mosheIserlish);
        Button yoelSirkisButt = (Button) findViewById(R.id.button_yoelSirkis);
        Button yomTuvLifmanHelerButt = (Button) findViewById(R.id.button_yomTuvLifmanHeler);
        Button natanNetaShapiraButt = (Button) findViewById(R.id.button_natanNetaShapira);
        Button yehushuhaHarifButt = (Button) findViewById(R.id.button_yehushuhaHarif);
        Button klonimosKalmanButt = (Button) findViewById(R.id.button_klonimosKalman);
        Button yosefNehemiaKorinetzerButt = (Button) findViewById(R.id.button_yosefNehemiaKorinetzer);
        Button mosheFridmanButt = (Button) findViewById(R.id.button_mosheFridman);

        //open PDF handle
        mosheIserlishButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_mosheIserlish.pdf");
            }
        });

        //open PDF handle
        yoelSirkisButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_yoelSirkis.pdf");
            }
        });

        //open PDF handle
        yomTuvLifmanHelerButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_yomTuvLifmanHeler.pdf");
            }
        });

        //open PDF handle
        natanNetaShapiraButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_natanNetaShapira.pdf");
            }
        });

        //open PDF handle
        yehushuhaHarifButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_yehushuhaHarif.pdf");
            }
        });

        //open PDF handle
        klonimosKalmanButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_klonimosKalman.pdf");
            }
        });

        //open PDF handle
        yosefNehemiaKorinetzerButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_yosefNehemiaKorinetzer.pdf");
            }
        });

        //open PDF handle
        mosheFridmanButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_krakau_mosheFridman.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(RKrakauActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
