package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class DinimActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dinim);

        Button m01Butt = (Button) findViewById(R.id.button_dm01);
        Button m02Butt = (Button) findViewById(R.id.button_dm02);
        Button m03Butt = (Button) findViewById(R.id.button_dm03);
        Button m04Butt = (Button) findViewById(R.id.button_dm04);
        Button m05Butt = (Button) findViewById(R.id.button_dm05);
        Button m06Butt = (Button) findViewById(R.id.button_dm06);
        Button m07Butt = (Button) findViewById(R.id.button_dm07);
        Button m08Butt = (Button) findViewById(R.id.button_dm08);
        Button m09Butt = (Button) findViewById(R.id.button_dm09);
        Button m10Butt = (Button) findViewById(R.id.button_dm10);
        Button m11Butt = (Button) findViewById(R.id.button_dm11);
        Button m12Butt = (Button) findViewById(R.id.button_dm12);

        m01Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_01.pdf");
            }
        });

        m02Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_02.pdf");
            }
        });

        m03Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_03.pdf");
            }
        });

        m04Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_04.pdf");
            }
        });

        m05Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_05.pdf");
            }
        });

        m06Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_06.pdf");
            }
        });

        m07Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_07.pdf");
            }
        });

        m08Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_08.pdf");
            }
        });

        m09Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_09.pdf");
            }
        });

        m10Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_10.pdf");
            }
        });

        m11Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_11.pdf");
            }
        });

        m12Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("dinim_12.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(DinimActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
