package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RWarszawaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rwarszawa);

        Button shlomoZalmanLifshitzButt = (Button) findViewById(R.id.button_shlomoZalmanLifshitz);
        Button naftaliTzviYehudaButt = (Button) findViewById(R.id.button_naftaliTzviYehuda);
        Button haimSoluveichikButt = (Button) findViewById(R.id.button_haimSoluveichik);
        Button israelTaobButt = (Button) findViewById(R.id.button_israelTaob);
        Button ichakNisenboimButt = (Button) findViewById(R.id.button_ichakNisenboim);
        Button avrahamMordecaiButt = (Button) findViewById(R.id.button_avrahamMordecai);
        Button kalmanKalmishShapiraButt = (Button) findViewById(R.id.button_kalmanKalmishShapira);
        Button menachemZambaButt = (Button) findViewById(R.id.button_menachemZamba);
        Button meirBalavanButt = (Button) findViewById(R.id.button_meirBalavan);

        //open PDF handle
        shlomoZalmanLifshitzButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_shlomoZalmanLifshitz.pdf");
            }
        });

        //open PDF handle
        naftaliTzviYehudaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_naftaliTzviYehuda.pdf");
            }
        });

        //open PDF handle
        haimSoluveichikButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_haimSoluveichik.pdf");
            }
        });

        //open PDF handle
        israelTaobButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_israelTaob.pdf");
            }
        });

        //open PDF handle
        ichakNisenboimButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_ichakNisenboim.pdf");
            }
        });

        //open PDF handle
        avrahamMordecaiButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_avrahamMordecai.pdf");
            }
        });

        //open PDF handle
        kalmanKalmishShapiraButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_kalmanKalmishShapira.pdf");
            }
        });

        //open PDF handle
        menachemZambaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_menachemZamba.pdf");
            }
        });

        //open PDF handle
        meirBalavanButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_warszawa_meirBalavan.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(RWarszawaActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
