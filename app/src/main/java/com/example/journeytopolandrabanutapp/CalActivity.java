package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal);

        Button y2019Butt = (Button) findViewById(R.id.button_cy2019);
        Button y2020Butt = (Button) findViewById(R.id.button_cy2020);
        Button y2021Butt = (Button) findViewById(R.id.button_cy2021);
        Button y2022Butt = (Button) findViewById(R.id.button_cy2022);

        y2019Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cal_2019.pdf");
            }
        });

        y2020Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cal_2020.pdf");

            }
        });

        y2021Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cal_2021.pdf");

            }
        });

        y2022Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cal_2022.pdf");

            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(CalActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
