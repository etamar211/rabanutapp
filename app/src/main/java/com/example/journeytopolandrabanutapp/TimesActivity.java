package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class TimesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_times);

        Button m09Butt = (Button) findViewById(R.id.button_tm09);
        Button m10Butt = (Button) findViewById(R.id.button_tm10);
        Button m11Butt = (Button) findViewById(R.id.button_tm11);
        Button m12Butt = (Button) findViewById(R.id.button_tm12);

        m09Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("times_09.pdf");
            }
        });

        m10Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("times_10.pdf");
            }
        });

        m11Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("times_11.pdf");
            }
        });

        m12Butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("times_12.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(TimesActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
