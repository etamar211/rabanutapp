package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CohenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cohen);

        Button okopowaButt = (Button) findViewById(R.id.button_okopowa);
        Button lupuchovButt = (Button) findViewById(R.id.button_lupuchov);
        Button treblinkaButt = (Button) findViewById(R.id.button_treblinka);
        Button rakowickiButt = (Button) findViewById(R.id.button_rakowicki);
        Button mapButt = (Button) findViewById(R.id.button_map);
        Button birkenauButt = (Button) findViewById(R.id.button_birkenau);
        Button auschwitzButt = (Button) findViewById(R.id.button_auschwitz);
        Button krakauButt = (Button) findViewById(R.id.button_krakau);
        Button kielceButt = (Button) findViewById(R.id.button_kielce);
        Button majdanekButt = (Button) findViewById(R.id.button_majdanek);
        Button warszawaButt = (Button) findViewById(R.id.button_warszawa);


        okopowaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_okopowa.pdf");
            }
        });

        lupuchovButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_lupuchov.pdf");
            }
        });

        treblinkaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_treblinka.pdf");
            }
        });

        rakowickiButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_rakowicki.pdf");
            }
        });

        mapButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_map.pdf");
            }
        });

        birkenauButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_birkenau.pdf");
            }
        });

        auschwitzButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_auschwitz.pdf");
            }
        });

        krakauButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_krakau.pdf");
            }
        });

        kielceButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_kielce.pdf");
            }
        });

        majdanekButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_majdanek.pdf");
            }
        });

        warszawaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("cohen_warszawa.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(CohenActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
