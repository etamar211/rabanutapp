package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RabanimActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rabanim);

        Button gurButt = (Button) findViewById(R.id.button_rGur);
        Button warszawaButt = (Button) findViewById(R.id.button_rWarszawa);
        Button luvlinButt = (Button) findViewById(R.id.button_rLuvlin);
        Button lizankButt = (Button) findViewById(R.id.button_rLizank);
        Button kotchkButt = (Button) findViewById(R.id.button_rKotchk);
        Button krakauButt = (Button) findViewById(R.id.button_rKrakau);

        gurButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent gurIntent = new Intent(RabanimActivity.this, RGurActivity.class);
                startActivity(gurIntent);
            }
        });

        warszawaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent warszawaIntent = new Intent(RabanimActivity.this, RWarszawaActivity.class);
                startActivity(warszawaIntent);
            }
        });

        luvlinButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent luvlinIntent = new Intent(RabanimActivity.this, RLuvlinActivity.class);
                startActivity(luvlinIntent);
            }
        });

        krakauButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent krakauIntent = new Intent(RabanimActivity.this, RKrakauActivity.class);
                startActivity(krakauIntent);
            }
        });

        //open PDF handle
        kotchkButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_kotchk.pdf");
            }
        });

        //open PDF handle
        lizankButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_lizank.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(RabanimActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
