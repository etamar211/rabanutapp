package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RLuvlinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rluvlin);

        Button shalomShechnaButt = (Button) findViewById(R.id.button_shalomShechna);
        Button meirMeluvlinButt = (Button) findViewById(R.id.button_meirMeluvlin);
        Button shlomoLuryaButt = (Button) findViewById(R.id.button_shlomoLurya);
        Button azrielHurvichButt = (Button) findViewById(R.id.button_azrielHurvich);
        Button tzadokHacohenMeluvlinButt = (Button) findViewById(R.id.button_tzadokHacohenMeluvlin);
        Button yaakovIchakHaleviHurvichButt = (Button) findViewById(R.id.button_yaakovIchakHaleviHurvich);
        Button meirShapiraButt = (Button) findViewById(R.id.button_meirShapira);
        Button yaakovBenYosefPolackButt = (Button) findViewById(R.id.button_yaakovBenYosefPolack);
        Button yaakovIchakRabinovichButt = (Button) findViewById(R.id.button_yaakovIchakRabinovich);
        Button simchaBonimMepashischaButt = (Button) findViewById(R.id.button_simchaBonimMepashischa);

        //open PDF handle
        shalomShechnaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_shalomShechna.pdf");

            }
        });

        //open PDF handle
        meirMeluvlinButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_meirMeluvlin.pdf");
            }
        });

        //open PDF handle
        shlomoLuryaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_shlomoLurya.pdf");
            }
        });

        //open PDF handle
        azrielHurvichButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_azrielHurvich.pdf");
            }
        });

        //open PDF handle
        tzadokHacohenMeluvlinButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_tzadokHacohenMeluvlin.pdf");
            }
        });

        //open PDF handle
        yaakovIchakHaleviHurvichButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_yaakovIchakHaleviHurvich.pdf");
            }
        });

        //open PDF handle
        meirShapiraButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_meirShapira.pdf");
            }
        });

        //open PDF handle
        yaakovBenYosefPolackButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_yaakovBenYosefPolack.pdf");
            }
        });

        //open PDF handle
        yaakovIchakRabinovichButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_yaakovIchakRabinovich.pdf");
            }
        });

        //open PDF handle
        simchaBonimMepashischaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_luvlin_simchaBonimMepashischa.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(RLuvlinActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
