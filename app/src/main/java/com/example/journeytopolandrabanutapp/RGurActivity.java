package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RGurActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgur);

        Button ichakMeirAlterButt = (Button) findViewById(R.id.button_ichakMeirAlter);
        Button yehudaArieLivAlterButt = (Button) findViewById(R.id.button_yehudaArieLivAlter);
        Button avrahamMordechaiAlterButt = (Button) findViewById(R.id.button_avrahamMordechaiAlter);

        //open PDF handle
        ichakMeirAlterButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_gur_ichakMeirAlter.pdf");
            }
        });

        //open PDF handle
        yehudaArieLivAlterButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_gur_yehudaArieLivAlter.pdf");
            }
        });

        //open PDF handle
        avrahamMordechaiAlterButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("rabanim_gur_avrahamMordechaiAlter.pdf");
            }
        });
    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(RGurActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }
}
