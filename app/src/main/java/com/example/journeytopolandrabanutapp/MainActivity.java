package com.example.journeytopolandrabanutapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * This activiy will be used as the main menu.
 * @author Etamar Cohen
 * @email etamar211@gmail.com
 * Date 02.06.19
 * Version 1.0
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button prayersButt = (Button) findViewById(R.id.button_prayers);
        Button kashrotButt = (Button) findViewById(R.id.button_kashrot);
        Button cohenButt = (Button) findViewById(R.id.button_cohen);
        Button calButt = (Button) findViewById(R.id.button_cal);
        Button hadrachaButt = (Button) findViewById(R.id.button_hadracha);
        Button timesButt = (Button) findViewById(R.id.button_times);
        Button dinimButt = (Button) findViewById(R.id.button_dinim);
        Button mashovButt = (Button) findViewById(R.id.button_mashov);
        Button rabanimButt = (Button) findViewById(R.id.button_rabanim);


        //open the prayers PDF
        prayersButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("prayers.pdf");
            }
        });

        //open the kashrot PDF
        kashrotButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("kashrot.pdf");
            }
        });

        //open the cohen activity
        cohenButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cohenIntent = new Intent(MainActivity.this, CohenActivity.class);
                startActivity(cohenIntent);
            }
        });

        //open the calendar activity
        calButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent calIntent = new Intent(MainActivity.this, CalActivity.class);
                startActivity(calIntent);
            }
        });

        //open the hadracha PDF
        hadrachaButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPdfFile("hadracha.pdf");
            }
        });

        //open the times activity
        timesButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent timesIntent = new Intent(MainActivity.this, TimesActivity.class);
                startActivity(timesIntent);
            }
        });

        //open the dinim activity
        dinimButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent dinimIntent = new Intent(MainActivity.this, DinimActivity.class);
                startActivity(dinimIntent);
            }
        });

        //open the mashov URL in default web browser
        mashovButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }
        });

        //open the rabanim activity
        rabanimButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent rabanimIntent = new Intent(MainActivity.this, RabanimActivity.class);
                startActivity(rabanimIntent);
            }
        });

    }

    public void openPdfFile(String filePath){
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.parse(PipeProvider.CONTENT_URI + filePath),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(MainActivity.this, "כדי להשתמש באפלקצייה זו צריך שיהיה מותקן אפלקציית PDF. אנא ההתקן כזאת", Toast.LENGTH_LONG).show();
        }
    }

}
